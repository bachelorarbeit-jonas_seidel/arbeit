\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage[mathcal]{euscript}
\usepackage{fancyvrb}
\usepackage{multicol}
\usepackage{lipsum}
\usepackage{contour}
\usepackage{ulem}
\usepackage{dsfont}
\usepackage{textcomp}

\usepackage{algorithm}
\usepackage{algpseudocode}

\renewcommand{\ULdepth}{1.8pt}
\contourlength{0.8pt}

\newcommand{\myuline}[1]{%
  \uline{\phantom{#1}}%
  \llap{\contour{white}{#1}}%
}


\usepackage[a4paper,textwidth=16.75cm, top=2.5cm, bottom=3cm]{geometry}
\usepackage[hidelinks = true]{hyperref}
\usepackage{filecontents}
\usepackage{pgfplots}
\pgfplotsset{width=.4\textwidth,compat=newest} 
%\usepgfplotslibrary{external}
\usepackage{tikz}
\usetikzlibrary{positioning, shapes}
%\tikzexternalize{main}

\usepackage{xcolor}
\usepackage{adjustbox}

\newcommand\selectvara{\adjustbox{cfbox=blue,bgcolor=blue!5}}
\definecolor{byzantium}{rgb}{0.44, 0.16, 0.39}
\newcommand\selectvarb{\adjustbox{cfbox=byzantium,bgcolor=byzantium!5}}
\definecolor{byzantine}{rgb}{0.74, 0.2, 0.64}
\newcommand\selectvarc{\adjustbox{cfbox=byzantine,bgcolor=byzantine!5}}

\usepackage{longtable}
\usepackage{arydshln}

\setlength{\columnsep}{.6cm}

\usepackage{fancyhdr}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,RO]{The Flow Maximizing Network Maintenance Problem}
\fancyhead[RE,LO]{Jonas Seidel}
\fancyfoot[CE,CO]{\leftmark}
\fancyfoot[RE,RO]{\thepage}

\usepackage{amsthm}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}

\theoremstyle{remark}
\newtheorem*{remark}{Remark}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}

\renewcommand\qedsymbol{$\square$}


%Math Operators
\DeclareMathOperator{\proj}{proj}
\DeclareMathOperator{\poly}{poly}
\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\hull}{hull}
\DeclareMathOperator{\lprelax}{lprelax}
\DeclareMathOperator{\eq}{eq}
\DeclareMathOperator{\rnk}{rnk}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\conv}{conv}
\DeclareMathOperator{\st}{s.t.}
\DeclareMathOperator*{\argmin}{\arg\min}
\DeclareMathOperator*{\argmax}{\arg\max}
\DeclareMathOperator{\maxflow}{maxflow}
\DeclareMathOperator{\impl}{impl}
\DeclareMathOperator{\dual}{dual}
\DeclareMathOperator{\optval}{optval}
\DeclareMathOperator{\optsol}{optsol}
\DeclareMathOperator{\lhsmat}{lhsmat}
\DeclareMathOperator{\rhsvect}{rhsvect}
\DeclareMathOperator{\optdir}{optdir}
\DeclareMathOperator{\activeeq}{active}
\DeclareMathOperator{\globactiveeq}{globactive}

%Shorthands / subject to change
\def\capturet{projected}
\def\patht{\texttt{abating}}
\newcommand{\enumn}[1]{\left[#1\right]}
\def\multicolseparator{\noindent\makebox[\textwidth][c]{\rule{\textwidth}{.4pt}}\vspace*{.25cm}}
\def\NMP{\mathcal{NMP}(\mathcal N,\mathrm T, \widetilde A)}
\def\CMP{\mathcal{NMP}(\mathcal C,\mathrm T, \widetilde A)}
\def\CMPc{\mathcal{NMP}(\mathcal C,\mathrm T, \widetilde{\mathcal C})}
\def\N{\mathcal N = \left((V,A),c,v_s,v_t)\right)}
\def\C{\mathcal C = \left((V,A),c,v_s,v_t)\right)}

\begin{document}
	
	
	
	\begin{titlepage}
		\begin{center}
			\vspace*{1cm}
			
			\Huge
			\textbf{The Flow Maximizing Network Maintenance Problem}
			
			\vspace{0.5cm}
			\LARGE
			An Integer Linear Programming Approach
			\vspace{1.5cm}
			
			Bachelor Thesis of\\
			\vspace{.25cm}
			\textbf{Jonas Seidel}
			
			\vfill
			
			
			{\large Primary Examiner: Prof. Dr. Ir. Arie M.C.A. Koster\\
			Secondary Examiner: JProf. Dr. rer. nat. Christina Büsing}
			
			\vspace{0.8cm}
			
			\includegraphics[width=0.1\textwidth]{rwth_mathe2_rgb_crop.png}
			
			\Large
			Lehrstuhl II für Mathematik\\
			Rheinisch-Westfälische Technische Hochschule Aachen\\
			March $18^{th}$ of $2021$
			
		\end{center}
	\end{titlepage}
	
	\iffalse
	{\tt
	Words and symbols written in \verb|\ttfamily| Teletype fonts are subject to change / entirely temporary.\\

	In the following $(\mathcal{N}, c, \mathrm{T})$, $(\mathcal{C}, c, \mathrm{T})$ will describe a $\mathcal{NMP}$-instance and $\mathrm{X}, x, f$ will be both used to give relations between variables of the examined polyeder and as concrete values. Context will hopefully make their meaning apparent.\\

	We assume integer capacities, though a generalization to rational ones is possible.\\

	Simple Complements will we substituted by \verb|\setminus| in the future
	}
	\fi
	
	\def\phif#1{\prescript{\mathcal I}{}{\Phi}_{#1}^\mathcal C}
	\def\phig#1{\prescript{}{}{\Phi}_{#1}^\mathcal C}
	\def\psig#1{\Psi_{#1}^\mathcal C}
	\def\lambdag#1{\Lambda_{#1}^\mathcal C}
	\def\cf{\prescript{\mathcal I}{}{c}_{}^\mathcal C}
	\def\cg{\prescript{}{}{c}_{}^\mathcal C}
	\def\tf{\prescript{\mathcal I}{}{t^*}_{}^\mathcal C}
	\def\tg{\prescript{}{}{t^*}_{}^\mathcal C}
	
	\tableofcontents
	\newpage
	\section{Introduction}
	\input{Foreword.tex}
	\newpage
	\section{Problem}
	\input{Problem.tex}
	\newpage
	\section{Mixed Integer Linear Programming}
	\input{Mixed Integer Linear Programming.tex}
	\newpage
	\section{Standard Program}
	\input{Standard Formulation.tex}
	\newpage
	\section{Benders Decomposition}
	\input{Benders Decomposition.tex}
	\subsection{Benders Applied to the Maintenance Problem}
	\input{MP Benders.tex}
	\newpage
	\section{Performance Properties}
	\input{Performance Properties.tex}
	\subsection{Testing Methodology}
	\input{Testing Methodology.tex}
	\input{Test Evaluation.tex}
	\newpage
	\section{Valid Inequalities}
	\label{valid_inequalities}
	\input{Valid Inequalities.tex}
	\subsection{General Properties}
	\input{General Properties.tex}
	\newpage
	\subsection{Capture Inequalities}
	\label{capture_inequalities_chapter}
	\input{Capture Inequalities.tex}
	\newpage
	\section{Separation Heuristics for $\poly\NMP$}
	\label{sepheur}
	\input{Separation Heuristics.tex}
	\subsection{The Stacking Approach}
	\input{Separation Heuristics iterative_capture_inequalities.tex}
	%cut strengthening in arbitrary graphs
	\newpage
	\section{SAT on $\phig{\cdot}$}
	\label{sat_on_phi}
	\input{SAT on phi.tex}
	\newpage
	\section{Shortcomings and Suggestions for Future Research}
	\label{future}
	\input{Future Research.tex}
	\newpage
	\section{Glossary of Notation}
	\input{glossary.tex}
	\label{glossary}
	\newpage
	\section*{Used Materials}
	\begin{itemize}
		\item The used code, data set and various other materials employed in the making of this thesis can be found at \url{https://git.rwth-aachen.de/bachelorarbeit-jonas_seidel}
	\end{itemize}
	\begin{thebibliography}{99}
		\bibitem{overview} 
		Steven Chien, Kyriacos Mouskos \textit{Optimizing Work Zones for Highway Maintenance with Floating Car Data (FCD)} University Transportation Reserch Center (New Jersey Institute of Technology) (2015)
		\bibitem{power} A. Abiri-Jahromi, M. Fotuhi-Firuzabad and E. Abbasi \textit{An Efficient Mixed-Integer Linear Formulation for Long-Term Overhead Lines Maintenance Scheduling in Power Distribution Systems} IEEE TRANSACTIONS ON POWER DELIVERY, VOL. 24, NO. 4 (2009)
		\bibitem{flowpavement} Aditya Medurya, Samer Madanat \textit{Incorporating network considerations into pavement management systems: A case for approximate dynamic programming} Transportation Research Part C (2013)
		\bibitem{railaggrflow} Natashia Boland, Thomas Kalinowski, Hamish Waterer, Lanbo Zheng \textit{Mixed integer programming based maintenance scheduling for the Hunter Valley coal chain} Journal of Scheduling (2013)
		\bibitem{genetic} T. F. Fwa, W.T. Chan and K. Z. Hoque \textit{NETWORK  LEVEL  PROGRAMMING  FOR  PAVEMENT  MANAGEMENT  USING GENETIC ALGORITHMS} 4$^{th}$ International Conference on Managing Pavements (1998)
		\bibitem{hshaap} Hendrik Schaap \textit{Ganzzahlige Optimierungsansätze zur Planung von Autobahnsanierungsmaßnahmen} (2016) [Master thesis submitted to Lehrstuhl II für Mathematik, RWTH (unpublished)]
		\bibitem{merz} Lotta Merz \textit{Optimierung aufeinanderfolgender maximaler
Flüsse eines Autobahnnetzwerkes mit
vorgesehenen Kapazitätseinschränkungen} (2018) [Bachelor thesis submitted to Lehrstuhl II für Mathematik, RWTH (unpublished)]
		\bibitem{kleinmanns} Nick Kleinmanns \textit{Ansätze zur zeitlichen Verteilung
von Autobahnbaustellen mithilfe
von Min-Cost-Flow} (2020) [Bachelor thesis submitted to Lehrstuhl II für Mathematik, RWTH (unpublished)]
		\bibitem{scip} Gerald Gamrath and Daniel Anderson and Ksenia Bestuzheva and Wei-Kun Chen and Leon Eifler and Maxime Gasse and Patrick Gemander and Ambros Gleixner and Leona Gottwald and Katrin Halbig and Gregor Hendel and Christopher Hojny and Thorsten Koch and Le Bodic, Pierre and Stephen J. Maher and Frederic Matter and Matthias Miltenberger and Erik M{\"u}hmer and Benjamin M{\"u}ller and Marc E. Pfetsch and Franziska Schl{\"o}sser and Felipe Serrano and Yuji Shinano and Christine Tawfik and Stefan Vigerske and Fabian Wegscheider and Dieter Weninger and Jakob Witzig \textit{The SCIP Optimization Suite 7.0} Optimization Online (2020)
		\bibitem{polymake} Benjamin Assarf, Ewgenij Gawrilow, Katrin Herr, Michael Joswig, Benjamin Lorenz, Andreas Paffenholz, Thomas Rehn \textit{Computing convex hulls and counting integer points with polymake} Mathematical Programming Computation (2017)
		\bibitem{karloff} Howard Karloff \textit{Linear Programming} Birkhäuser (1991)
		\bibitem{Schrijver} Alexander Schrijver \textit{THEORY OF LINEAR AND INTEGER PROGRAMMING} Wiley-Interscience (1998)
		\bibitem{LP2} George B. Dantzig, Mukund N. Thapa \textit{Linear Programming:
2: Theory and Extensions} Springer-Verlag New York (2003)
		%\bibitem{BendersReview} Ragheb Rahmaniania, Teodor Gabriel Crainica, Michel Gendreaua, Walter Rei \textit{The Benders decomposition algorithm: A literature review} European Journal of Operational Research (2017)
	\end{thebibliography}
\end{document}
