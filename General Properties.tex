In order for the following assertions to be true, we will need some rather weak assumptions. Namely we assume, that all capacities are strictly positive, that at least two epochs are allotted for maintenance and lastly that all nodes and edges lie on a valid $s-t$ path.


\begin{proposition}
	\label{polydim}
	Under the above assumptions it holds, that $\dim\poly\NMP = \mathrm T (|A| + |\widetilde A|) +1-|\widetilde A|-(|V|-2)\mathrm T$, where $\mathcal{N} = ((V,A),c,v_s,v_t)$.
\end{proposition}
\begin{proof}
	We note that by our definition it suffices to show $\rnk(\lhsmat_{\globactiveeq}\poly\NMP)  = |\widetilde A|+(|V|-2)\mathrm T$, as $\mathrm T (|A| + |\widetilde A|) +1$ trivially gives the dimension of the encompassing vector space, meaning the number of present variables. Thus it will suffice to show that only the equality constraints are active for all points and that these constraints are linearly independent.\par\medskip
	
	Regarding "exclusively equality constraints are active": We have assumed that $T > 1$ and thus for every $(a,t) \in \widetilde A \times \mathrm T$ neither $0 \geq x_a^t$ nor $x_a^t \leq 1$ is active in all solutions. For this consider those solutions that allocate all arcs $\widetilde A$ to be maintained in the first epoch and those for which all critical edges are maintained in the second epoch.\par\smallskip
	Now we note that also the bounds on the flow variables and capacity constraints cannot be active in all points, because, among other things, we assumed positive capacities: Regarding the lower bounds we note, that all edges are assumed to lie on some $s-t$ path. Since every path is augmenting to the $0$-flow during some epoch if all maintenance operations are assigned to a single epoch, we may assign positive flow to the path's edges during some other epoch. Regarding the capacity constraints we note that in the same fashion a solution can be found in which the upper bound is non-zero for all edges during some epoch, but the flow is $0$ on all edges.\par\smallskip
	Concerning those constraints bounding the target variable from above we note, that due to the lack of lower bounds on mentioned target variable this constraint obviously is not active in all solutions.\par\bigskip
	Having thus proven the first part it remains to be shown, that the normal vectors to the equality constraints induces hyperspaces are indeed linearly independent. To reiterate, these constraints are given by the capacity and maintenance assignment constraints:
	\begin{alignat*}{4}
	&			& \sum_{\mathclap{a \in \delta^+(v)}} f_a^t - \sum_{\mathclap{a \in \delta^-(v)}} f_a^t &= 0	\quad	&& \forall v \in V\setminus\{s,t\}, t \in \enumn{\mathrm T} \tag{cap}\\
	&			& \sum_{t \in \enumn{\mathrm T}} x_a^t &= 1 															&& \forall a \in \widetilde A\tag{massign}
	\end{alignat*}
	Since the two classes of constraints feature disjoint sets of variables, we may examine their linear independence separately. In fact all members of the assignment constraint class have pairwise disjoint variables, meaning that the corresponding normal vectors are trivially linearly independent and the same holds for the subsets for every epoch of conservation constraints.\par\smallskip 
	Now assume that the left hand sides of flow conservation constraints for a certain epoch are linearly dependent. We thus get a non-empty sum of constraints, which cancels out all present variables. Normalize the coefficient so one constraint only contributes $1$'s. We note that all contained edge-flow variables need to cancel out. Looking at a singular edge-flow variable, we contend that due to this variable only being present in two constraints, those constraints of adjacent nodes to our starting constraints node must necessarily feature in our sum with the same coefficients. Therefore inductively our sum has to contain all nodes in the weak component. However not all nodes of this component have corresponding constraints. Meaning that the incident edges of source and target nodes, which are in said weak component by assumption (all nodes lie on some $s-t$ path), won't be possible to cancel out in this fashion. We thus get a contradiction to our shown induction hypothesis. Therefore the above mentioned constraints are indeed linearly independent.
\end{proof}

\begin{definition}[Primary Standard Form]
	A valid inequality $l + r^\top x \geq \lambda \mathrm X - s^\top f$ of $\poly\NMP$ is said to be in primary standard form \texttt{iff} $\forall a \in \widetilde A \colon \left(r_a \in \mathbb R_{\geq 0}^{\mathrm T} \land \exists t \in \enumn{\mathrm T}\colon r_a^t = 0\right)$
\end{definition}\medskip

We can easily transform an arbitrary inequality to primary standard form by taking advantage of the maintenance assignment constraints.\medskip

\begin{proposition}
	\label{validform}
	A valid inequality $l + r^\top x \geq \lambda \mathrm X - s^\top f$ in primary standard form of $\poly\NMP$  fulfills $l, a, \lambda \geq 0$
\end{proposition}
\begin{proof}
	Concerning \underline{$\lambda\geq 0$} we note, that if not we get $\lambda < 0$. Now since $\mathrm X$ free and no other constraint bounds this variable from below we can decrease it in any solution and still maintain feasibility in $\poly\NMP$. However this will eventually break the inequality, which is a contradiction.\par\medskip
	\underline{$r \geq 0$} holds by assumption of primary normal form.\par\medskip
	For \underline{$l,r \geq 0$} we register, that by assumption of primary normal form we know that for all $a \in \widetilde A$ there exits $t(a) \in \enumn{\mathrm T}$ such, that $r_a^{t(a)} = 0$. Choose $x$ now in a way such, that $\forall a \in \widetilde A \colon x_a^{t(a)} = 1$ and otherwise $0$. We thus have selected a maintenance epochs for all critical edges and therefore attain the feasible point $(0,x,0) \in \poly\NMP$. Note that when substituting this point into our facet we yield $l \geq 0$ which is what was to be shown.
\end{proof}

\begin{proposition}
	\label{cutfacetform}
	A facet $\mathcal{F}$ and its supporting inequality $l + r^\top x \geq \lambda\mathrm X - s^\top f$ in primary standard form of $\poly\CMP$, where $\C$ is a cut network and $\lambda \not= 0$ fulfills $l, a, \lambda, s \geq 0$
\end{proposition}
\begin{proof}
	For $l,a,\lambda \geq 0$ see \ref{validform}.\par\smallskip
	With respect to \underline{$s \geq 0$} we see that if not then there would exist $(a,t)\in $ with $s_a^t < 0$. We claim that this in turn would necessitate $\forall (\mathrm X, x, f) \in \mathcal F \colon f_a^t = c_a(1-x_a^t)$, since otherwise it would be possible to increase the flow along $a$ during epoch $t$ in such a solution, which would break the inequality whilst preserving feasibility, which stand in contradiction to our supporting inequality being valid.\par\medskip
	We can therefore assume $\forall (\mathrm X, x, f) \in \mathcal F \colon f_a^t = c_a(1-x_a^t)$. Now bound $\dim \mathcal F$ by determining the number of linearly independent normal vector to hyperplanes containing $\mathcal F$. Consider the following constraints known to be active for all points of $\mathcal F$:
	\begin{alignat}{4}
	&			& \sum_{\mathclap{\mathfrak a \in \delta^+(v)}} f_{\mathfrak a}^t - \sum_{\mathclap{\mathfrak a \in \delta^-(v)}} f_{\mathfrak a}^t &= 0	\quad	&& \forall v \in V\setminus\{s,t\}, t \in \enumn{\mathrm T} \\
	&			& \sum_{t \in \enumn{\mathrm T}} x_{\mathfrak a}^t &= 1 															&& \forall \mathfrak a \in \widetilde A\\
	&			& f_a^t &= c_a(1-x_a^t)\quad\quad\\
	&			& l + r^\top x &= \lambda\mathrm X - s^\top f\quad\quad
	\end{alignat}
	We claim that all of the normal vectors to above hyperplanes are linearly independent. Assuming linear dependence (4) has to have coefficient $0$ since otherwise the resulting sum would non-zero value in component corresponding to variable $\mathrm X$. Further (3) has to have coefficient 0 because if it contributes to the sum some non-zero amount in the coordinate of $x_a^t$ then this which will need to be canceled out by some vector in the span of (2)'s and (1)'s normal vectors. However these inequalities will have an integer multiple of $\mathrm T$ many pairwise different components of "$x$" variables "$\not= 0$", meaning that this cancellation is impossible. Now we have arrived at the case already discussed in the proof of \ref{polydim}. We attain the linear independence of the named inequalities. Therefore conclude that $\dim \mathcal F \leq \mathrm T (|A| + |\widetilde A|) +1-|\widetilde A|-(|V|-2)\mathrm T - 2 = \dim \poly \NMP - 2$ so $\mathcal F$ is evidently not a facet. Therefore by contradiction $s_a^t < 0$ cannot be.
\end{proof}