Let our assumptions from the previous chapters continue to apply. Consider our \textit{Flow Level Decomposition} (\ref{flow_level_decomp}) proposition. In the following we will try to separate subsets of inequalities that fulfill this necessary condition for facets. When trying to mimic \ref{pos_logic_bound} as closely as possible, one may come up with what we will call the three-pronged variant.

\subsubsection{Three-Pronged Separation}
\iffalse
\begin{alignat*}{4}
	\mathfrak A_\mathcal C(x)\colon \quad	&& \sum_{\mathfrak f \in \enumn{m}} \quad	&\min \quad	&&x^\top a(\mathfrak f) && < 0\ \mathbb{?}\\
							&& 											&\begin{aligned}s.t.\\\phantom{s.t.}\end{aligned} \quad	&&\begin{rcases}\left(\proj_e \mathbb I\right)^\top d \geq 1\\a(\mathfrak f)_e \geq (1-\mathbb I_e)d_e, \forall e \in \mathcal C\end{rcases}, \quad &&\forall\ \hspace{3.4cm}\mathclap{\mathbb I\text{ implicant of }\Psi_{\mathfrak f}^{\mathcal C} \text{ with } \mathfrak f > \sum_{i = 1}^{\mathfrak f-1} \left(1-\mathbb I\right)^\top a(i)}\\TODO: is this already a heuristic could it be that one that a implicant that is by this metric not bounded is actually bounded because those subcases ubounded are infeasible?
							&& 											&			&&\ a(\mathfrak f) \in \{0,1\}^{\mathcal C\times \enumn{\mathrm T}}, d \in \{0,1\}^{\mathcal C}
\end{alignat*}
\fi

Let $\mathfrak f \in \mathbb{N}$ and $\mathbb I = \prod_{(a,t) \in \mathfrak s \subseteq \widetilde{\mathcal C} \times \enumn{\mathrm T}} \varphi_a^t \prod_{(a,t) \in \overline{\mathfrak s} \subseteq \widetilde{\mathcal C} \times \enumn{\mathrm T}}  \overline{\varphi_a^t}$ be an implicant of $\phig{\mathfrak f}\land\psig{}$. We define $x(\mathbb I) \in \{0,1\}^{\widetilde{\mathcal C}\times\enumn{\mathrm T}}$ by $\forall (a,t) \in \widetilde{\mathcal C}\times \enumn{\mathrm T} \colon x(\mathbb{I})_a^t = 1 \iff (a,t) \in \mathfrak s$.
Now we can denote the following separation problem in a compact fashion. \par\medskip
Let $(\widehat{\mathrm X}, \widehat x)$ be the to-be-separated point in $\proj_{({\mathrm X}, x_{\widetilde{\mathcal C}})}\poly\lprelax\NMP$. Assume we are given some $\big(a(i)\big)_{i \in \enumn{\mathfrak f-1}}$ already fulfilling $\forall x \in \proj_x \dom \CMPc \colon \sum_{i \in \enumn{\mathfrak f-1}} a(i)^\top x \geq \sum_{i \in \enumn{\mathfrak f-1}} \phig{i}(x)$. We note, that using
\begin{alignat*}{20}
\mathfrak A_{\mathcal C, \mathfrak f}(\widehat x)\colon \quad	&& &&\min \quad	&\widehat x^\top a(\mathfrak f) &&\\
&& 											&&s.t.\quad &x(\mathbb I)^\top a(\mathfrak f) \geq 1 \quad &&\forall\ \mathbb I\text{ implicant of } \psig{\mathfrak f}\land\psig{} \text{ with } \mathfrak f > \sum_{i = 1}^{\mathfrak f-1} a(i)^\top x(\mathbb I)\\
&& 											&&			&\ a(\mathfrak f) \in \{0,1\}^{\widetilde{\mathcal C}\times \enumn{\mathrm T}}
\end{alignat*}
we can determine an inclusion minimal $a(\mathfrak f)$ for which $(a(\mathfrak i))_{i \in \enumn{\mathfrak f}}$ collectively satisfy this inequality for this additional summand $\mathfrak f$, meaning $\forall x \in \proj_x \dom \CMPc \colon \sum_{i \in \enumn{\mathfrak f}} a(i)^\top x \geq \sum_{i \in \enumn{\mathfrak f}} \phig{i}(x)$. We see that, if after calculating all $m$ levels the resulting summed up minima $\big(\sum_{i \in \enumn{m}} a(i)\big)^\top \widehat x$ are less than $\widehat{\mathrm X}$, then we have found a valid inequality that separates $(\widehat{\mathrm X},\widehat x) \in \proj_{({\mathrm X}, x)} \poly \lprelax \NMP$ according to \ref{capture_logic_ident}, \ref{capture_ineq_valid} and our analysis in \ref{pos_logic_bound}. We note at this point, that this might not be able to separate all \capturet{} capture inequalities, since our locally minimal decisions (solutions of a certain flow amount) may turn out to be globally (meaning among all valid lhs for this $\widehat x$) suboptimal.\par\medskip
While an explicit calculation of all inequalities present in $\mathfrak A_{\mathcal C, \mathfrak f}$ is possible, it may be advisable to start introducing separation problems recursively. In order to determine the implicants of constraints violated by some $a(\mathfrak f)$ in the $\mathfrak f$-th program, we can employ
\begin{alignat*}{20}
\mathfrak B_{\mathcal{C},\mathfrak f, (a(i))_{i \in \enumn{\mathfrak f-1}}}(a(\mathfrak f))\colon \quad	&&  \min \quad	&a(\mathfrak f)^\top \mathbb I && < 1\ ?\\
						&&											s.t.\quad 	& \sum_{a \in \mathcal L} (1-\mathbb I_a^t) \geq 1 \quad &&\forall t \in \enumn{\mathrm T} \quad \forall \mathcal L \subseteq \widetilde{\mathcal C}, c(\mathcal L) > c(\mathcal C) - \mathfrak f \tag{log-cap-set}\\
						&&														& \sum_{t \in \enumn{\mathrm T}} \mathbb I_a^t \geq 1 && \forall a \in \mathcal C\\
						&&														& \mathfrak f-1 \geq \sum_{i = 1}^{\mathfrak f-1} a(i)^\top \mathbb I\\
						&&														& \mathbb I \in \{0,1\}^{\widetilde{\mathcal C}\times \enumn{\mathrm T}}
\end{alignat*}
and in turn log-cap-set inequalities of this program can be separated by using
\begin{alignat*}{20}
	\mathfrak C_{\mathcal C,\mathfrak f}(\mathbb I^t)\colon \quad	&&\min \quad 	& \sum_{a \in \mathcal L} {\mathbb I_a^t}^\top \mathcal L 				&& < 1\ ?\\
	&& s.t.\quad	&c^\top \mathcal L \geq c(\mathcal C) - (\mathfrak f-1)\quad 	&&\\
	&& 				&\mathcal L \in \{0,1\}^{\widetilde{\mathcal C}}
\end{alignat*}
which can be reduced to the knapsack problem when using the complementary set.\par\medskip
Now multiple problems should have become obvious. The most glaring of which is probably, that $m$ is required to determine when to stop calculating new $a$'s. For applications we will have to retreat to the usage of our current dual bound $d$, as it is by definition the best known upper bound of $m$ at the point of separation. The resulting inequalities are still going to be valid according to \ref{cut_ineq_validity}. We can also terminate early if $\mathfrak A_{\mathcal C, \mathfrak f}(\widehat x)$ is infeasible, where $(\widehat{\mathrm X},\widehat x)$ is the to be separated point in $\proj_{({\mathrm X}, x)} \poly\NMP$. In this case the dual bound will need to be updated, as the optimal value $m$ will necessarily be less than what can flow through our selected cut.\par\medskip
Furthermore the above should have made apparent, that using this separation heuristic is ill-advised when one is confronted with the task of allocating maintenance periods in a cut network. This is because we effectively have solved the problem by the time separation will be over, as we will have determined $m$ once a infeasible program is found. Solutions can then be derived from the present inequalities. A different approach will need to be derived for these problems.

\subsubsection{Capture-Set Separation}
\label{cap-set_sepa}
We want to present another much less complex method. For this, let $(\widehat{\mathrm X},\widehat{x})$ continue to describe the to-be-separated-point. We utilize $\mathfrak C_{\mathcal C, \mathfrak f}$ in the following way:

\begin{alignat*}{20}
\min_{t \in \enumn{\mathrm T}}\mathfrak C_{\mathcal C,\mathfrak f}(\widehat x^t) \quad =	\quad &&\min_{t \in \enumn{\mathrm T}}\ \min \quad 	& \sum_{a \in \mathcal L}  ({\widehat x}_a^t)^\top\mathcal L 				&& \eqqcolon \quad {\mathcal L(\mathfrak f)}^\top {\widehat x^{t^*(\mathfrak f)}}\\
&& s.t.\quad	&c^\top \mathcal L \geq c(\mathcal C) - (\mathfrak f-1)\quad 	&&\\
&& 				&\mathcal L \in \{0,1\}^{\widetilde{\mathcal C}}
\end{alignat*}

If indeed $\sum_{i \in \enumn{m}}{\mathcal L(i)}^\top {\widehat x^{t^*(i)}} < \mathrm X$, then we have found a valid-yet-violated inequality $\sum_{i \in \enumn{m}}{\mathcal L(i)}^\top {\widehat x^{t^*(i)}} \geq \mathrm X$ by the same logic that we mentioned when discussing \ref{logic_ident_global}.\par\medskip
We call this separated sub-class of capture inequalities capture-set inequalities and note, that in the example given at the end of \ref{capture_inequalities_chapter} both our derived inequality and the alternative choice, that would have resulted in a facet, are in fact capture-set inequalities.\par\medskip
Contrary to three-pronged separation, this has the benefit of reducing to $m\cdot\mathrm T$ knapsack problems for which we can employ specialized solvers. Furthermore these problems are completely independent from one another and although we will not take advantage of this, it is possible to adjust this problem to use differing cuts for the $\mathrm T$ different knapsack instances and still attain a valid inequality as a result. This may be in the hopes of improving ones chances of finding violated inequalities, for instance by choosing current Min-Cut of the separated point for every epoch or by choosing cuts that in some sense maximize fractionality of the $x$-variables contained therein.

\subsection{Stacking Implementation}

\subsubsection{Initial Determination}
\label{sep_alg}
Let $\mathcal{O}\colon \enumn{d} \longrightarrow \{0,1\}^{\widetilde{\mathcal{C}}}\times\enumn{\mathrm T}$ be a function that satisfies $\big(\sum_{i = 1}^{d} \mathcal O(i)\big)^\top x \geq \mathrm X$ being a valid inequality. Then for some $(a,t)\in \widetilde{\mathcal{C}}\times \enumn{\mathrm T}$ we may choose to model $\big(\sum_{i = 1}^{d} \mathcal O(i)\big)^\top \widehat x$ as an integral since $\big(\sum_{i = 1}^{d} \mathcal O(i)\big)^\top \widehat x = \int_{\mathbb N_0} \mathcal O(\cdot)^\top \widehat x\ d\#$, where $\#$ is the counting measure. One may therefore approach calculation by using step functions to approximate. Consider the following algorithm:\par\bigskip

We aim to introduce new steps where they most appear to matter. To estimate their use we interpolate linearly between known values. This is unfounded (see \ref{future}).\par\medskip
We will be continuously evaluating $\mathcal{O}$ at these additional points using the newly attained information to update lower and upper bounds accordingly. Termination occurs if our bounds betray that no violated inequality exists, or that one has been found.\par\medskip
To be more specific consider:
\begin{algorithm}
	\caption{\textit{lhs}-Iterative-Approx}
	\begin{algorithmic}[1]
		\Procedure{approx}{$\mathcal O, \mathrm X, d$}
		\State $S \coloneqq \{\}$ an ordered map
		\State $lb \coloneqq \mathcal O(1)\cdot (d-1) + \mathcal O(d)$
		\State $ub \coloneqq \mathcal O(d)\cdot (d-1) + \mathcal O(1)$
		\State insert $\left(1,\Big\lceil\frac{d+1}{2}\Big\rceil, d\right)$ into $S$ with key $(d-2)\cdot(\mathcal O(u)- \mathcal O(l))$
		\While{$\mathrm X \in (lb, ub]$, $S \not = \{\}$}
			\State remove the largest element (by key) $(l,c,u)$ from $S$
			\State $lb \coloneqq lb - (u-c)\cdot\mathcal O(u) + \mathcal O(c)$
			\State $ub \coloneqq ub - (c-l)\cdot\mathcal O(l) + \mathcal O(c)$
			\If{$c-l-1 > 0 \land \mathcal O(c) - \mathcal O(l) > 0$}
				\State insert $\left(l,\Big\lceil\frac{l+c}{2}\Big\rceil, c\right)$ into $S$ with key $(c-l-1)\cdot(\mathcal O(c)-\mathcal O(l))$
			\EndIf
			\If{$u-c-1 > 0 \land \mathcal O(u) - \mathcal O(c) > 0$}
				\State insert $\left(c,\Big\lceil\frac{c+u}{2}\Big\rceil, u\right)$ into $S$ with key $(u-c-1)\cdot(\mathcal O(c)- \mathcal O(l))$
			\EndIf
		\EndWhile
		\If{$ub < \mathrm X$}
		introduce $\sum_{i = 1}^{d} \mathcal{O}(i)^\top x \geq \mathrm X$ into our formulation
		\EndIf
		\If{$lb \geq \mathrm X$}
		separation has failed perhaps try some other method
		\EndIf
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

It should be obvious, that a function describing the evaluation of our described capture-set separation problem at different flow amounts can be used as such a $\mathcal{O}$ without any special considerations. \par\medskip
For the three-pronged approach we however note, that the cross dependence among the programs for different flow amounts can lead to our lower bound being invalidated. Less serious but still relevant is, that coefficient solutions to a some flow level $\mathfrak f$ may be invalidated if lower levels make improvement to their coefficient selection. This is because fewer coefficients on these lower levels force additional inequalities to be present in programs for $\mathfrak A_{\mathcal C, \mathfrak f}(\widehat x)$, as we need to compensate for some cases no longer being bounded by the lower level coefficients. This propagation problem should be mitigated by saving our evaluation state for a previously evaluated separation problem and re-optimizing with the new inequalities.\par\medskip
Using the two methods after one another would thus still enable us to use our lower bounds to detect the absence of capture-set inequalities early while also separating a greater class of inequalities. We therefore suggest the above \textit{lhs}-Iterative-Approx algorithm with an additional attempt to improve the found coefficients using our three-pronged separation, that is at least if no valid inequality has been found after our main loop has exited. During this we no longer have the benefit of lower bounds and therefore no clear indication of whether our calculation is actually likely to result in a separating cut. Since an exhaustive calculation for all levels is likely computationally infeasible or at least undesirable, we will need some other trigger for termination, the development of which we leave for the future (see \ref{future}). We will require however, that before termination there was at least one constraint present in one of the evaluated $\mathfrak A_{\mathcal C, \cdot}(\widehat x)$.
\subsubsection{Dual Bound Propagation}
\label{dual_bound_propagation}
If after successfully generating a separating cut in the fashion outlined above one where to hold onto the information which coefficients where necessary for the different flow amounts, then we can strengthen the inequality later on once improved dual bounds have been determined. This can be done by simply removing these no-longer-necessary coefficient from the inequality, since according to \ref{cut_ineq_validity} the inequality will remain valid.

\subsection{Defense of Efficacy}

As mentioned at the beginning of this chapter, our utilization of $\mathcal{NP}$-hard problems for separation is a significant shortcoming of our approach. While this may preclude this algorithm from use when it comes to arbitrary instances, however, as we have seen in \ref{elongated_network_complexity}, there are instances of significant complexity where inclusion minimal cuts are only a small fraction of the original networks size. This would mean, that compared to the runtime of solving the entire instance, separating in the way outlined above may become justified.\par\medskip
Having made this argument, we would be remiss in not mentioning separation using optimization over cut-subinstances: Meaning by the same logic, if inclusion minimal cuts are comparably small, yet many are relevant in the sense outlined in \ref{elongated_network_complexity}, it may become beneficial to use the following separation program:
\begin{alignat*}{20}
	\min \quad& \widehat x^\top \lambda \quad < \widehat{\mathrm X} \ ?\\
	s.t. \quad& \mathfrak x^\top \lambda \geq \min\{d,\mathfrak X\} \quad &&\forall ({\mathfrak X}, \mathfrak x) \in \proj_{({\mathrm X}, x)}\poly\mathcal{NMP}(\mathcal C, \mathrm T, \widetilde{\mathcal{C}})
\end{alignat*}
which itself would use the separation program
\begin{alignat*}{20}
\min \quad	& \mathfrak x^\top \lambda - \mathfrak X\quad < 0\ ?\\
s.t. \quad	& ({\mathfrak X}, \mathfrak x) \in \proj_{({\mathrm X}, x)}\poly\mathcal{NMP}(\mathcal C, \mathrm T, \widetilde{\mathcal{C}})\\
			& \phantom{(}\mathfrak X \leq d
\end{alignat*}
Our data suggests, that since this is a cut network, the standard program may be preferable to our benders decomposition in this separation application. We have to note however, that this programs objective direction differs form those programs tested. \par\bigskip
We also cannot make an informed determination on whether this method or our previously derived stacking approach is more efficient at this time (see \ref{future}). However both have their benefits and drawbacks: As we have mentioned in \ref{dual_bound_propagation}, when our dual bound is improved, we can easily strengthen those inequalities, which were derived using three-pronged-separation. While this strengthening is also possible using this new method we would have to re-optimize. Note that although our primary separation program is a LP we may need to separate additional inequalities using the above MIP. This operation is therefore associated with significant overhead. \par\medskip
One benefit can be found in the greater class of separated inequalities. Indeed all inequalities relevant to the convex hull of $\proj_x \dom \mathcal{NMP}(\mathcal C, \mathrm T, \widetilde{\mathcal{C}})$ can be determined. Namely these are all \capturet{} capture inequalities. We have previously left open how to select the cuts over which to separate these inequalities. This separation approach may thus be employed to find out, which cut determination algorithms produce cuts relevant to the problem of the entire network, making this separation approach, if nothing else, at least relevant as a tool to study our problem further.
\par\bigskip
%\verb|column generation to evade greedy drawbacks|\par
