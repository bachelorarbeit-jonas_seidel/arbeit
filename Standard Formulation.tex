We now have the necessary tools to give a mixed integer linear programming formulation of our problem:\par\bigskip

Let $(\mathcal N,\mathrm T, \widetilde A)$, where $\N$, be a maintenance problem instance. The objective is to find a mapping $w: \widetilde A \longrightarrow [\mathrm T]$ such that $\min_{t \in [\mathrm T]} \maxflow((V,A \setminus w^{-1}(\{t\})),c,v_s,v_t)$ is maximal. We need to model the possible values, that the preimage of $w$ can assume, with variables. We do this by introducing $\vert \widetilde A\vert$ binary variables for all $\mathrm T$ many possible preimages. We denote the variable for $a \in \widetilde A, t \in \enumn{\mathrm T}$ by $x_a^t$. Collectively $x^t$ can be interpreted as an indicator vector of $w^{-1}(\{t\}) \subseteq \widetilde A$.\par\bigskip

We note that a function is uniquely identified by the preimages to singleton subsets of its codomain. Further our indicator vectors identify valid preimage functions so long as every domain element appears in exactly one of the singletons preimages sets.\par\medskip

To be explicit we can regain this function from its encoding \texttt{"}$x$\texttt{"} in the following way:
\begin{proof}
	We define $i\colon \left\{\mathfrak x \in \{0,1\}^{\widetilde A\times\enumn{\mathrm T}}\colon \sum_{t \in \enumn{\mathrm T}} \mathfrak x_a^t = 1\right\} \longrightarrow \enumn{\mathrm T}^{\widetilde A}, \mathfrak x \longmapsto \mathfrak w$, where $w(l) \coloneqq \argmax_{t \in \enumn{\mathrm T}} x_l^t$. Since the $\argmax$ will be uniquely identified by choice of our domain the function will be well defined. 
	
	\noindent Regarding surjectivity of $i$ we note that for $w\in \enumn{\mathrm T}^{\widetilde A}$ we choose $x$ such that $x_a = e_{w(a)}^\top$, the standard $w(a)$-th basis vector, for arbitrary $a\in \widetilde A$. Now it follows that $i(x)(a) = \argmax_{\mathfrak t \in \enumn{\mathrm T}} x^\mathfrak t_a = \argmax_{\mathfrak t \in \enumn{\mathrm T}} (e_{w(a)})_t = w(a)$, hence because $a$ arbitrary $i(x) = w$.\par
	\noindent Concerning injectivity we just note that if $x, y$ elements of the domain of $i$ differ, then they differ is some row $a \in \widetilde A$, meaning, that the induced functions differ for said arc.
\end{proof}

Thus the following holds:

\begin{alignat*}{4}
		&&\max_{\mathclap{w\colon \widetilde A \rightarrow \enumn{\mathrm T}}} \quad & \min\limits_{t \in [\mathrm T]} \maxflow\!\big((V,A \setminus w^{-1}(\{t\})),c,v_s,v_t\big)\\
&=\quad	&\max \quad	& \mathrm X\\
		&&s.t. \quad	& \mathrm X \leq \maxflow\!\big((V,A \setminus \{a\in \widetilde A \colon  x^t_a = 1\}),c,v_s,v_t\big)\quad \quad && \forall t \in \enumn{\mathrm T}\\
						&&& \sum_{\mathclap{t \in \enumn{\mathrm T}}} x_a^t = 1 && \forall a \in \widetilde A\\
						&&& x \in \{0,1\}^{\widetilde A \times \enumn{\mathrm T}}
\end{alignat*}

We now insert variables $f_a^t, a \in A, t \in \enumn{\mathrm T}$ to describe the flow over arc $a$ during epoch $t$. Consider the program


\begin{alignat*}{20}
\mathcal{NMP}(\mathcal N, \mathrm T, \widetilde A)\colon \quad
&&\max \quad	& \mathrm X\\
&&s.t.\quad 		& \mathrm X \leq \sum_{a \in \delta^+(v_s)} f_a^t														&& \forall t \in \enumn{\mathrm T}\tag{tvbound}\\
&&			& \sum_{\mathclap{a \in \delta^+(v)}} f_a^t - \sum_{\mathclap{a \in \delta^-(v)}} f_a^t = 0	\quad\quad	&& \forall v \in V\setminus\{v_s,v_t\}, t \in \enumn{\mathrm T} \tag{con} \\
&&			& f_a^t \leq (1-x_a^t) \cdot c_a 																				&& \forall a \in A, t \in \enumn{T} \tag{cap} \\
&&			& \sum_{t \in \enumn{\mathrm T}} x_a^t = 1 															&& \forall a \in \widetilde A\tag{massign}\\
&&			& x \in \{0,1\}^{\widetilde A \times \enumn{\mathrm T}}, {f \in \mathbb R_{\geq 0}^{A \times \enumn{\mathrm T}}}
\end{alignat*}

flow values $f^t, t \in \enumn{\mathrm T}$ of feasible points $(\mathrm X, x, f)$ describe flows in $\big((V,A \setminus \{a\in \widetilde A \colon  x^t_a = 1\}),c,v_s,v_t\big)$ by virtue of fulfilling the necessary constraints (cap) and (cons). We term the novel constraint classes (tvbound) and (massign) target variable bound constraints and maintenance assignment constraints respectively.\par\bigskip
\noindent Aiming to show that this is a valid MIP formulation of our problem we consider an optimal $w$. By its optimality there exist flows $\hat f^1,..,\hat f^T$ in the corresponding networks such that 
$\max\limits_{w\colon A \rightarrow \enumn{\mathrm T}} \min\limits_{t \in \enumn{\mathrm T}} \maxflow((V,A \setminus w^{-1}(\{t\})),c,v_s,v_t) = \min_{t\in \enumn{\mathrm T}} \sum_{a \in \delta^+(v_s)} \hat f^t_a \eqqcolon \hat{\mathrm X}$. This implies that $(\hat{\mathrm X}, i^{-1}(w), \hat f)$ admissible in the above program. \par\bigskip
\noindent Further for an optimal solution of the MIP $(\hat{\mathrm X}, \hat x, \hat f)$ it follows, that $\hat{\mathrm X}$ is a lower bound on $\min_{t \in \enumn{\mathrm T}} \maxflow((V,A \setminus i(\hat x)^{-1}(\{t\})),c,v_s,v_t)$ by definition meaning in particular of the optimal value of our problem.\par\bigskip
We attain that both instances, meaning our maintenance problem instance and the mixed integer linear programming instance, have coinciding optimal values.