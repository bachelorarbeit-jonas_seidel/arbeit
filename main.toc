\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Problem}{3}{section.2}%
\contentsline {section}{\numberline {3}Mixed Integer Linear Programming}{4}{section.3}%
\contentsline {section}{\numberline {4}Standard Program}{6}{section.4}%
\contentsline {section}{\numberline {5}Benders Decomposition}{8}{section.5}%
\contentsline {subsection}{\numberline {5.1}Benders Applied to the Maintenance Problem}{9}{subsection.5.1}%
\contentsline {section}{\numberline {6}Performance Properties}{11}{section.6}%
\contentsline {subsection}{\numberline {6.1}Testing Methodology}{11}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Test Evaluation}{11}{subsection.6.2}%
\contentsline {subsubsection}{\numberline {6.2.1}{\small \textit {nodes}} vs. {\small \textit {edges}}}{11}{subsubsection.6.2.1}%
\contentsline {subsubsection}{\numberline {6.2.2}{\small \textit {steps}} vs. {\small \textit {epochs}}}{12}{subsubsection.6.2.2}%
\contentsline {subsubsection}{\numberline {6.2.3}{\small \textit {cut networks}}}{14}{subsubsection.6.2.3}%
\contentsline {subsubsection}{\numberline {6.2.4}{\small \textit {share of critical}}}{15}{subsubsection.6.2.4}%
\contentsline {subsubsection}{\numberline {6.2.5}{\small \textit {decentralization}} vs. {\small \textit {connector capacity}}}{17}{subsubsection.6.2.5}%
\contentsline {subsection}{\numberline {6.3}Conclusions}{17}{subsection.6.3}%
\contentsline {section}{\numberline {7}Valid Inequalities}{19}{section.7}%
\contentsline {subsection}{\numberline {7.1}General Properties}{19}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Capture Inequalities}{21}{subsection.7.2}%
\contentsline {section}{\numberline {8}Separation Heuristics for $\poly \EuScript {NMP}(\EuScript N,\mathrm T, \setbox \z@ \hbox {\mathsurround \z@ $\textstyle A$}\mathaccent "0365{A})$}{31}{section.8}%
\contentsline {subsection}{\numberline {8.1}The Stacking Approach}{31}{subsection.8.1}%
\contentsline {subsubsection}{\numberline {8.1.1}Three-Pronged Separation}{31}{subsubsection.8.1.1}%
\contentsline {subsubsection}{\numberline {8.1.2}Capture-Set Separation}{32}{subsubsection.8.1.2}%
\contentsline {subsection}{\numberline {8.2}Stacking Implementation}{32}{subsection.8.2}%
\contentsline {subsubsection}{\numberline {8.2.1}Initial Determination}{32}{subsubsection.8.2.1}%
\contentsline {subsubsection}{\numberline {8.2.2}Dual Bound Propagation}{33}{subsubsection.8.2.2}%
\contentsline {subsection}{\numberline {8.3}Defense of Efficacy}{34}{subsection.8.3}%
\contentsline {section}{\numberline {9}SAT on $\prescript {}{}{\Phi }_{\cdot }^\EuScript C$}{35}{section.9}%
\contentsline {subsection}{\numberline {9.1}Heuristic Employment}{35}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Combating the "Tailing-Off" Effect}{35}{subsection.9.2}%
\contentsline {section}{\numberline {10}Shortcomings and Suggestions for Future Research}{36}{section.10}%
\contentsline {subsection}{\numberline {10.1}Theory}{36}{subsection.10.1}%
\contentsline {subsection}{\numberline {10.2}Data}{36}{subsection.10.2}%
\contentsline {section}{\numberline {11}Glossary of Notation}{37}{section.11}%
