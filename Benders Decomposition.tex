The classical benders algorithm provides an alternative approach to solving programs of the form\par
\begin{alignat*}{20}
&\mathcal O\colon\quad 	&\max \quad	& c_x^\top x \quad					&& + \quad													&&	c_y^\top y\\
&												&s.t. \quad	& \mathfrak{X}_1 x 					&& +  															&& 	\mathfrak Yy	&&\leq\quad &&r_1 \\
&												&						& \mathfrak{X}_2 x 					&&																	&& 									&&\leq  		&&r_2\\
&												&						& x \in \mathbb{N}_0^{n_x}, &&&&y \in \mathbb R_{\geq 0}^{n_y}
\end{alignat*}
where we require $\mathfrak X_1 \in \mathbb R^{l\times n_x}, \mathfrak X_2 \in \mathbb R^{s\times n_x}, \mathfrak Y \in \mathbb R^{l\times n_y}, l,s,n_x,n_y \in \mathbb N_0$ and $c_x \in \mathbb R^{n_x}, c_y \in \mathbb R^{n_y}$\par
\noindent Meaning programs that both integer and continuous variables are present.\par\medskip
\noindent The algorithm can be thought of as first decomposing the above program in the following way:\par\medskip
It trivially holds, that\par
\noindent\begin{minipage}[t]{.45\textwidth}
	\begin{alignat*}{20}
	\max \quad	& c_x^\top x \quad					&& + \quad													&&	c_y^\top y\\
	s.t. \quad	& \mathfrak{X}_1 x 					&& +  															&& 	\mathfrak Yy	&&\leq\quad &&r_1 \\
							& \mathfrak{X}_2 x 					&&																	&& 									&&\leq  		&&r_2\\
							& x \in \mathbb{N}_0^{n_x}, &&&&y \in \mathbb R_{\geq 0}^{n_y}
	\end{alignat*}
\end{minipage}
\begin{minipage}[t]{.1\textwidth}
	\begin{align*}
	=\\
	\\
	\\
	\end{align*}
\end{minipage}
\begin{minipage}[t]{.45\textwidth}
	\begin{alignat*}{20}
	\max \quad	& c_x^\top x \quad					&& + \quad	& \max\quad	&	c_y^\top y\\
	s.t. \quad	& \mathfrak{X}_2 x \leq r_2	&& 					& s.t.\quad &\mathfrak{X}_1 x 	+	\mathfrak Yy	&&\leq\ &&r_1 \\
							& x \in \mathbb{N}_0^{n_x}	&&					&						&y \in \mathbb R_{\geq 0}^{n_y}\\[-6pt]
	\end{alignat*}
\end{minipage}\par\bigskip
\noindent In the following we refer to the latter two problems as the primary and secondary problem. These two are to be read as being nested within one another, since the second clearly is dependent on the variables of the first. In fact we signal this in our notation
by denoting the secondary problem as $\mathcal S(\widehat x), \widehat x \in \mathbb R^{n_x}$ and calling it the dependent program. For the purposes of this discussion if $\widehat x \in \mathbb N_0^{n_x}, \mathfrak X_2 \widehat x \leq r_2$ then we call such a point admissible for the entire problem \texttt{iff} $\mathcal S(x)$ feasible and semi-admissible \texttt{iff} either $\mathcal S(x)$ or $\dual \mathcal S(x)$ is feasible.
The intention here being that $\widehat x$ ends up being admissible in this way \texttt{iff} $\widehat x \in \proj_x \dom \mathcal O$. $\proj_x \dom \mathcal O$ being the orthogonal projections of solutions onto the variables of the primary problem, resulting in such points having counterparts of matching objective function value in the original polyhedron $\poly \mathcal O$. On the other hand if a point is semi-admissible we can assign a coherent value to it as outlined in \ref{coherent_val}. Namely if the primal $S(\widehat x)$ is infeasible we may assign $-\infty$ and if it is unbounded we choose $\infty$. The result is, that our two nested problems coincide in value with $\mathcal O$, if we regard infeasible maximizing programs to have the optimal value of $-\infty$.\par\smallskip
Assume now, that $\widehat x$ is indeed semi-admissible for the nested problems. For $\mathcal S(\widehat x)$ we may thus substitute its dual (see \ref{sduality}, \ref{coherent_val}). Note at this time, that the integer values $x$ of the primary problem are to be treated as constants in the dependent program and hence the constraints constant component, when distributed to the right hand side, are given by $r_1-\mathfrak X_1 x$. Thus the resulting duals domain is completely independent of the \texttt{"}x\texttt{"}-variables:\par
\noindent\begin{minipage}[t]{.45\textwidth}
	\begin{alignat*}{20}
		\max\quad	&	\smash{c_y^\top} y\\
		s.t.\quad &\mathfrak Y y 							&&\leq\quad &&r_1-\mathfrak{X}_1 \widehat x \\
							&y \in \mathbb R_{\geq 0}^{n_y}
	\end{alignat*}
\end{minipage}
\begin{minipage}[t]{.1\textwidth}
	\begin{align*}
	=\\
	\\
	\end{align*}
\end{minipage}
\begin{minipage}[t]{.45\textwidth}
	\begin{alignat*}{20}
		\min\quad	&	\smash{\left(r_1-\mathfrak X_1 \widehat x\right)^\top}  d\\
		s.t.\quad & \smash{\mathfrak Y^\top}  d 										&&\geq\quad &&c_y \\
							& d \in \mathbb R_{\geq 0}^{l}
	\end{alignat*}
\end{minipage}\par\bigskip
Assuming now, that an optimal solution exists to $\dual \mathcal S(\widehat x)$:  It is commonly known that in this case, there exists an optimal extreme point$^{\cite{karloff} \text{ (thrm 10, red. to LPS possible)}}$ and hence $\optval \mathcal S(\widehat x) = \min\{(r_1 - \mathfrak X_1 x)^\top q \colon q \in Q \}$ for $Q$ being the set of extreme points. \par\medskip
If the opposite is the case, meaning $\mathcal S(\widehat x)$ has no optimal solution, then either...\par
\begin{itemize}
	\item[...] we attain infeasibility of $\dual \mathcal S(\widehat x)$, which would mean $\mathcal S(\widehat x)$ was unbounded (since our assumptions of semi-admissibility for $\widehat x$ preclude the possibility of both programs being infeasible) and can therefore terminate our calculations as the entire problem must then be unbounded. Or ...
	\item[...] we get unboundedness of $\dual \mathcal S(\widehat x)$ and thus the statement, that the set of dual extreme rays $P$ is non-empty.
\end{itemize}\par\medskip
As calculation can be stopped if the first case is achieved, we only need to concern ourselves with the second one. Assume from now on, that all $\{x \in \mathbb N_0\colon \mathfrak X_2 x \leq r_2\}$ are semi-admissible for the entire above problem like we previously assumed for just $\widehat x$. Therefore, like we previously attained for just $\mathcal S(\widehat x)$, we can apply the more general strong duality theorem \ref{coherent_val}. Consider the following program:\par
\begin{alignat*}{20}
  &\mathcal M_{prelim}:\quad  &\max \quad	& c_x^\top x + \optval\mathcal S(x)\\
  &                           &s.t. \quad	& \mathfrak{X}_2 x                                    &&\leq r_2 \\
  &                           &           & \left(r_1-\mathfrak X_1 x\right)^\top p             &&\geq 0,      \quad &&\forall p \in P \tag{feascuts}\\
  &                           &					  & x \in \mathbb{N}_0^{n_x}
\end{alignat*}\par\bigskip
For those elements $\bar x \in \{x \in \mathbb N_0^{n_x}\colon \mathfrak X_2  x \leq r_2\}\setminus\dom\mathcal M_{prelim}$ there obviously exists an extreme ray of negative objective function value in $\mathcal S(\bar x)$. Meaning we have only excluded those $x$ that would have triggered our second case. Furthermore for the remaining solutions there obviously doesn't exist any such ray, meaning if we choose our $\widehat x$ to be in $\dom \mathcal M_{prelim}$ either $\dual S(\widehat{x})$ is infeasible or our optimal solution is given by an extreme point.\par\medskip
Having thus excluded the second case, we can conclude $\optval \mathcal S(\widehat x) = \inf\{(r_1 - \mathfrak X_1 x)^\top q \colon q \in Q \}$. It consequently holds, that\par
\noindent\begin{minipage}[t]{.45\textwidth}
  \begin{alignat*}{20}
    &									    &\max \quad	& c_x^\top x + \mathcal S(x)\\
    &                     &s.t. \quad	& \mathfrak{X}_2 x                                    &&\leq r_2 \\
    &                     &           & \left(r_1-\mathfrak X_1 x\right)^\top p             &&\geq 0,      \quad &&\forall p \in P \\
    &                     &					  & x \in \mathbb{N}_0^{n_x}\\
    \\[4pt]
  \end{alignat*}
\end{minipage}
\begin{minipage}[t]{.1\textwidth}
	\begin{align*}
  =\\
	\\
	\\
	\\
	\\[4pt]
	\end{align*}
\end{minipage}
\begin{minipage}[t]{.45\textwidth}
  \begin{alignat*}{20}
    &                     &\max \quad	& c_x^\top x + \mathrm S\\
    &                     &s.t. \quad	& \mathfrak{X}_2 x                                    &&\leq r_2 \\
    &                     &           & \left(r_1-\mathfrak X_1 x\right)^\top p             &&\geq 0,           \quad &&\forall \quad		&& p \in P \\
	  &                   	&         	& \left(r_1-\mathfrak X_1 x\right)^\top q 										    &&\geq \mathrm S,  					\quad &&\forall 				&& q\in Q \tag{optcuts} \\
    &                     &					  & x \in \mathbb{N}_0^{n_x}
  \end{alignat*}
\end{minipage}\par\bigskip
\noindent Where we call the second of these programs the benders decomposition's master program $\mathcal M_{benders}(\mathcal O)$, or just master for short.
In the benders algorithm this program is solved by separation, meaning that our new constraint classes are being calculated as need be. This is done by subproblems, which are considered for some solution candidate $(\mathfrak x,\mathfrak S)$. The classical benders algorithm uses $\dual \mathcal S(\mathfrak x)$ to determine such broken feas- or optcuts.
If $\optval \dual \mathcal S(\mathfrak x) < \mathfrak S$, then some of these cuts are broken as determined by the attained solution extreme point or ray. Otherwise the point is feasible for the LP-relaxation.\par\medskip
%However there are certainly numerous spins on this algorithm utilizing similarly looking decompositions but still trying to mitigate its many short comings$^{\cite{BendersReview}}$.
